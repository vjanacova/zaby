

// žabia farma
//vsetky krmim rovnako
// ked ide program robit zabu, najskor spravi objekt typu zaba, 
// vyrobi tu konstrukciu podla funkcii (metod zadefinovanych v predpise)
// potom sa nad tym zapne constructor (to je funkcia) a nad tym za
class Zaba {
    constructor() {
        this._zjedene = 0;
        this.prezranie = 20; // tato zaba ma zadany pevny treshold kedy sa prezerie
    }
    ham(pocetmuch) {
        this._zjedene = this._zjedene + pocetmuch;
    }
    zije() {
        if (this._zjedene < this.prezranie) {
            return this._zjedene < this.prezranie; // to je funkcia ktorou zistujem ci zabicka zije, kedze si povem ak zaba zje viac ako 20 much, tak zomrie
        } else {
            return false;
        }

    }
}

// teraz spravim zabicku, ktorej pri jej vytvoreni poviem ze aku bude mat hranicu prezrania

class Ropucha {
    constructor(prepapanie) {
        this._zpapane = 0;
        this.prepapanie = prepapanie;
    }
    ham(muchy) {
        this._zpapane = this._zpapane + muchy;
    }
    zijes() {
        if (this._zpapane < this.prepapanie) {
            return true; // to je funkcia ktorou zistujem ci zabicka zije, kedze si povem ak zaba zje viac ako 20 much, tak zomrie
        } else {
            return false;
        }
    }

}

const Zabicka = new Zaba();
Zabicka.ham(10);
Zabicka.ham(12);
Zabicka.zije();
console.log(Zabicka.zije()); // takto 

const terarium = [];
for (let i = 0; i < 10; i++) {
    const z = new Zaba();
    terarium.push(z);
}

console.log(terarium);

// iteracia pre vsetky zabky akvaria

for (let i = 0; i < terarium.length; i++) {
    const zaba = terarium[i];
    zaba.ham(12);
    //keby som sem dala console.log(terarium) tak by som to spravila tak ze nakrmim zabu a dam pohlad do celeho akvaria a vypise mi po kazdej iteracii stav akvaria  
};

console.log(terarium);

// map - funkcia, ktor8 pomocou funkcie mapuje hodnoty pola na ine pole

const prezite = terarium.map(function (z) {
    return z.zije();
});

console.log(prezite);

//jednoduchsie iterovanie cez pole


terarium.forEach(function (zaba) {
    zaba.ham(4); 
    console.log (zaba.zije()); //hovorim ze pre kazdu zabu z teraria jej dam ham a potom nech mi vypise ci zije
});

console.log(terarium);

terarium.forEach(zaba => { //ak mam jeden atribut zaba, tak nemusim pisat function ale napisem =>
    zaba.ham(4); 
    console.log (zaba.zije()); //hovorim ze pre kazdu zabu z teraria jej dam ham a potom nech mi vypise ci zije
});

// a vyzorovo to vyzera super takto

terarium.forEach(zaba => zaba.ham(4));
   


const zijuce = terarium
    .map(zaba => zaba.zije())
    .filter(zije => zije == true)
    .length;

console.log(zijuce);

const zive = terarium
    .map(zaba => zaba.zije())
    .filter(zije => !zije) // kedze to zije je booleovska funkcia, tak tam staci to zije, ak by som chcela opak tak dam !zije
    .length;

console.log(zive);
