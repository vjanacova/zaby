class Zaba {
    constructor(prejedenie) {
        this.zjedene = 0;
        this.prejedenie = prejedenie;
    }
    ham(muchy) {
        this.zjedene = this.zjedene + muchy;

    }
    zijes() {
        if (this.zjedene < this.prejedenie) {
            return true; // to je funkcia ktorou zistujem ci zabicka zije, kedze si povem ak zaba zje viac ako 20 much, tak zomrie
        } else {
            return false;
        }
    }

}

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}
// generujem zaby, ktore maju roznu dobu zivota

const terarium = [];
for (let i = 0; i < 10; i++) {
    const zivot = getRandomInt(20);
    const zaba = new Zaba(zivot);
    terarium.push(zaba);
}

// 1. krmenie
terarium.forEach(zaba => { 
    zaba.ham(4); 
    console.log (zaba.zijes()); 
});

// 2. skontrolujem kolko zije
const zijuce = terarium
    .filter(zaba => zaba.zijes())
    .length;

console.log(zijuce);

