

//suma z pola cisel
function suma(pole) {
    let sum = 0;    //lebo zaciname od nuly a to treba zadefinovat
    for (let i = 0; i < pole.length; i++) {        //for i cyklus
        const cislo = pole[i];  //ity prvok pola je cislo a tu ho zadefinujes ako ked zratavas, tak cislo plus cislo je cislo
        sum = sum + cislo;
    }
    return sum;
}
const cisla = [1, 2, 3, 4, 5, 6, 7, 8, 9];
const sum1 = suma(cisla);   //tu si vypises vysledok z funkcie, treda ho priradit zas nejakej konštante

console.log(sum1);  //tu si ho vypises aby si ho videla na vystupe

