//Objekty
//okrem string, cisla, logickej hodnoty (true, false), kontajnerové typy - zoznamy a slovniky(objekty)
//zakladny konctrol flow sa riesil pomocou podmienok a cyklov
//objekty kombinujú stavove premenné a fcie

//atribúty: stavové premenné 
//metódy:objektové premenné - funkcie
//inštancie sú vyrobené podla predpisu triedy - ta opisuje aky bude objekt, je to "navod na výrobu"

//triedu zapisujeme takto
class Zaba {
    constructor(meno, zaludok) {         //toto priraduje atributy
        this.meno = meno;
        this.zaludok = 0;
    }
    hop() {     //metoda
        console.log("hopkám", this.meno);

    }
    ham(pocetmuch) {
        this.zaludok = this.zaludok + pocetmuch

    }
    kakaj(pocetmuch) {
        this.zaludok = this.zaludok - pocetmuch
    }
}
const žaba = new Zaba("Helga")     //identifikator premennej, instancia, vytvorí novy objekt
console.log(žaba);
žaba.hop()
žaba.ham(5)
žaba.ham(6)
žaba.kakaj(2)
console.log("žalúdok má v sebe", žaba.zaludok, "múch");


const žabiak = new Zaba("Ferdinand");
console.log(žabiak);
žabiak.hop()


// žabia farma
//vsetky krmim rovnako

class 