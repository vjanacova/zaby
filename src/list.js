// iteracia cez zoznam

const l = [1, 2, 3, "hello"];

for (let i = 0; i < l.length; i++) {
    const element = l[i];
    console.log(element);
}

//zoznam zoznamov

const g = [[1, 2], [3, 4]];

for (let i = 0; i < g.length; i++) {
    const element = g[i];
    console.log(element);
}

// pridať do prázdeho zoznamu

const a = [[1, 2], [3, 4]];
const d = [];

for (let i = 0; i < a.length; i++) {
    const matica = a[i];
    console.log(matica);
    d.push(matica);
}

console.log(d)


for (const matica of d) {
    console.log(matica);
}

