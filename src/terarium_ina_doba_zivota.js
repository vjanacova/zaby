//generator nahodnych cisel

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

console.log(getRandomInt(3));
// expected output: 0, 1 or 2

console.log(getRandomInt(1));
// expected output: 0

console.log(Math.random());
  // expected output: a number between 0 and 1


//mame zabicky, ktore budeme generovat tak, ze v konstruktore im musime zadat ako dlho budu zit (hranica prejedenia)
//doba zivota znamena ze ak zabicka zje nejaky pocet much zomrie

// takto tvorime zabu, ktorej ja presne poviem ake ma parametre 
class Ropucha {
    constructor(zpapane,prezranie) {
        this.zpapane = zpapane;
        this.prezranie = prezranie;
    }
    ham(muchy) {
        this.zpapane = this.zpapane + muchy;
    }
    zijes() {
        if (this.zpapane < this.prezranie) {
            return true; // to je funkcia ktorou zistujem ci zabicka zije, kedze si povem ak zaba zje viac ako 20 much, tak zomrie
        } else {
            return false;
        }
    }

}

//teraz to sprav9m tak, ze ked vytvorim zabu, bude mat nahodnu dobu zivota

class Zaba {
    constructor(prejedenie) {
        this.zjedene = 0;
        this.prejedenie = prejedenie;
    }
    ham(muchy) {
        this.zjedene = this.zjedene + muchy;

    }
    zijes() {
        if (this.zjedene < this.prejedenie) {
            return true; // to je funkcia ktorou zistujem ci zabicka zije, kedze si povem ak zaba zje viac ako 20 much, tak zomrie
        } else {
            return false;
        }
    }

}

// generujem zaby, ktore maju roznu dobu zivota

const terarium = [];
for (let i = 0; i < 10; i++) {
    const zivot = getRandomInt(20);
    const zaba = new Zaba(zivot);
    terarium.push(zaba);
}

// 1. krmenie
terarium.forEach(zaba => { 
    zaba.ham(4); 
    console.log (zaba.zijes()); 
});

// 2. skontrolujem kolko zije
const zijuce = terarium
    .map(zaba => zaba.zijes())
    .filter(zijes => zijes)
    .length;

console.log(zijuce);

// 3. chcem to spravit n krat=> nakrmit a skontrolovat
for (let i = 0; i < 10; i++) {
    terarium.forEach(zaba => { 
        zaba.ham(4); 
    });

    const zijuce = terarium
        .map(zaba => zaba.zijes())
        .filter(zijes => zijes)
        .length;

        console.log(i, zijuce); // i vypise kolkata je to iteracia a pocet zijucich
}

// https://www.w3schools.com/jsref/jsref_dowhile.asp
// ctrl + shift + r + oznaceny kod a urobi mi to z toho funkciu - refaktoring
function pocitanie() {
    return terarium
        .map(zaba => zaba.zijes())
        .filter(zijes => zijes)
        .length;
}
//takto bez map nerobim z boolinovskymi hodnotami, ale robim nad celym polom ziab

function pocitanie() {
    return terarium
        .filter(zaba => zaba.zijes())
        .length;
}

function Krmenie() {
    terarium.forEach(zaba => {
        zaba.ham(4);
    });
}

let prezivsie
do {
    Krmenie();

    prezivsie = pocitanie();

        console.log(prezivsie);
    
} while (prezivsie > 0)

