const o = { x: 1, y: 2 };
console.log(o.y); //vypise zo slovnika o iba prvok x - je to cez bodku
console.log(o["y"]); //aj takto to ide ale ten kľúč, ktorým mu poviem, ktorú hodnotu chcem je string; preto ten zapis

const keys = Object.keys(o); //Object.keys - vytiahla som zoznam kľúčov a teda je to list a nie dictionary a ma teda dlzku
console.log(keys);

for (i = 0; i < keys.length; i++) {
    const key = keys[i];
    console.log(key, o[key]);
}

//rýchlejšia verzia for 
for (const key in o) {
    console.log(key, o[key]);
}
