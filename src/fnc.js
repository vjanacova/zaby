
function log(text) {
    console.log(text);
}
// node prečíta ten kod a zacne ho interpretovať a za ním ide cesta
//takze sa to spusti : node src/fnc.js

log("textik")

//scitavacia fcia

const z = 2;
const y = 3;

function sucet(y, z) {
    return y + z;
}

const s = sucet(3, 5);
console.log(s);

log(sucet(6, 9));

log((sucet(4, 4)));

//suma z pola cisel
function suma(pole) {
    let sum = 0;    //lebo zaciname od nuly a to treba zadefinovat
    for (let i = 0; i < pole.length; i++) {        //for i cyklus
        const cislo = pole[i];  //ity prvok pola je cislo a tu ho zadefinujes ako ked zratavas, tak cislo plus cislo je cislo
        sum = sum + cislo;
    }
    return sum;
}
const cisla = [1, 2, 3, 4, 5, 6, 7, 8, 9];
const sum1 = suma(cisla);   //tu si vypises vysledok z funkcie, treda ho priradit zas nejakej konštante

console.log(sum1);  //tu si ho vypises aby si ho videla na vystupe

//suma z pola poli cisel
const matica = [[1, 2, 3, 4, 5], [12, 34, 56, 78, 90], [123, 134, 145, 156, 167]]

function sumapoli(polepoli) {
    let sum = 0;
    for (let i = 0; i < polepoli.length; i++) {
        const polecisel = polepoli[i];  //ity prvok pola nazvaneho polepoli je polecisel a tu si ho zadefinujes
        const sum2 = suma(polecisel);    // tu mame zadefinovane scitanie pomocou sumy definovanej vyssie, ako premennu sme tam dali polecisel
        sum = sum + sum2;       //a tymto menim premennu kde mi vpluje vysledok, ako x=x+1, kde 1 je ale dalsia suma z polacisel a x je vysledok po predchadzajucom scitani
    }
    return sum;     //tu mu prikazem aby vrátil vysledok
}
const sum = sumapoli(matica);       //zadefinovana vysledok sumy "sumapoli" z pola "matice" ako constanta "sum"
console.log(sum);       //vypíše constantu "sum"

// DU?? cez reduce?

